import 'package:flutter/material.dart';

class AbsorbPointerExample extends StatefulWidget {
  const AbsorbPointerExample() : super();

  @override
  AbsorbPointerExampleState createState() => AbsorbPointerExampleState();
}

class AbsorbPointerExampleState extends State<AbsorbPointerExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("AbsorbPointer"),
        ),
        body: Center(
          child: AbsorbPointer(
            /**
         * ? What is AbsorbPointer....
         * ! with the help of [AbsorbPointer] we can prevent the touch event from the user....
         */
            absorbing: true, // ! change here to activate the event....
            /**
         * ! When absorbing is true, this widget prevents its subtree from receiving pointer events by terminating hit testing at itself....
         * [absorbing] == true ? try to scroll the listview....
         */
            ignoringSemantics: false,
            /**
         * ! Whether the semantics of this render object is ignored when compiling the semantics tree....
         * ! if null takes the [absorbing] value....
         */
            child: ListView.builder(
              // ? here the ListView have scroll event....
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: RaisedButton(
                      // ? here the RaisedButton have click event....
                      child: Text("List item $index"),
                      onPressed: () {
                        print("List item $index");
                      }),
                );
              },
            ),
          ),
        ));
  }
}
