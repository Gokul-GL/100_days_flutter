import 'package:flutter/material.dart';

class TableExample extends StatefulWidget {
  const TableExample() : super();

  @override
  TableState createState() => TableState();
}

class TableState extends State<TableExample> {
  double iconSize = 40;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Table View Example"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(10.0),
              child: Table(
                border: TableBorder.all(),
                children: [
                  TableRow(children: [
                    Column(children: [
                      Icon(
                        Icons.account_box,
                        size: iconSize,
                      ),
                      Text('My Account')
                    ]),
                    Column(children: [
                      Icon(
                        Icons.settings,
                        size: iconSize,
                      ),
                      Text('Settings')
                    ]),
                    Column(children: [
                      Icon(
                        Icons.lightbulb_outline,
                        size: iconSize,
                      ),
                      Text('Ideas')
                    ]),
                  ]),
                  TableRow(children: [
                    Icon(
                      Icons.cake,
                      size: iconSize,
                    ),
                    Icon(
                      Icons.voice_chat,
                      size: iconSize,
                    ),
                    Icon(
                      Icons.add_location,
                      size: iconSize,
                    ),
                  ]),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
