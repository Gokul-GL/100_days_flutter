import 'package:flutter/material.dart';

class LimitedBoxPage extends StatefulWidget {
  const LimitedBoxPage() : super();

  @override
  LimitedBoxState createState() => LimitedBoxState();
}

class LimitedBoxState extends State<LimitedBoxPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LimitedBox(
        maxHeight: 100,
        child: Container(
          color: Colors.amberAccent,
        ),
      ),
    );
  }
}
