import 'package:flutter/material.dart';

class SizedBoxPage extends StatefulWidget {
  const SizedBoxPage() : super();

  @override
  SizedBoxPageState createState() => SizedBoxPageState();
}

class SizedBoxPageState extends State<SizedBoxPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          color: Colors.blueGrey,
          child: SizedBox(
            height: 200,
            width: 200,
            child: FlatButton(
                onPressed: () {
                  print("Cliked");
                },
                child: Text("Button")),
          ),
        ),
      ),
    );
  }
}
