import 'package:flutter/material.dart';

class FittedBoxExample extends StatefulWidget {
  const FittedBoxExample() : super();

  @override
  FittedBoxExampleState createState() => FittedBoxExampleState();
}

class FittedBoxExampleState extends State<FittedBoxExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Stack(
        children: <Widget>[
          // Container one

          Container(
            height: 200,
            width: 300,
            color: Colors.amberAccent,

            //this is used to fit the Container two on Container one

            child: FittedBox(
              child: Container(
                height: 300,
                width: 100,
                color: Colors.blueGrey,
              ),
            ),
          ),

          //Container two

          // Container(
          //   height: 300,
          //   width: 100,
          //   color: Colors.blueGrey,
          // ),
        ],
      ),
    ));
  }
}
