import 'package:flutter/material.dart';

class FadeImage extends StatefulWidget {
  const FadeImage() : super();
  @override
  FadeImageState createState() => FadeImageState();
}

class FadeImageState extends State<FadeImage> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("FadeInImage"),
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: FadeInImage.assetNetwork(
          placeholder: "images/loadingimagegif.gif",
          image:
              "https://i.pinimg.com/564x/e9/29/1c/e9291cc39e820cd4afc6e58618dfc9e0.jpg",
          width: size.width,
          //height: size.height,
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}
