import 'package:flutter/material.dart';

class AlignExample extends StatefulWidget {
  const AlignExample() : super();

  @override
  AlignExampleState createState() => AlignExampleState();
}

class AlignExampleState extends State<AlignExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      //color: Colors.indigoAccent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: Text("Alignment.center"),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Text("Alignment.topRight"),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Text("Alignment.topLeft"),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Text("Alignment.bottomRight"),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Text("Alignment.bottomCenter"),
          ),
        ],
      ),
    ));
  }
}
