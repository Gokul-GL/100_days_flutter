import 'package:flutter/material.dart';
import 'package:hundred_days_of_flutter_widget/fade_image.dart';

class ClipRRectExample extends StatefulWidget {
  const ClipRRectExample() : super();

  @override
  ClipRRectExampleState createState() => ClipRRectExampleState();
}

class ClipRRectExampleState extends State<ClipRRectExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ClipRRect"),
      ),
      // body: Center(
      //   child: Container(
      //     child: ClipRRect(
      //       borderRadius: BorderRadius.circular(50.0),
      //       child: FadeImage(),
      //     ),
      //   ),
      // ),

      // its to load the FadeImage Page in the ClipRect page

      body: Center(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(15.0),
          child: Container(
            // this alignment is used to align the fadeImage in this clip
            child: Align(
              alignment: Alignment.center,
              widthFactor: 0.75,
              heightFactor: 0.75,
              child: FadeImage(),
            ),
          ),
        ),
      ),
    );
  }
}
