import 'package:flutter/material.dart';

class DraggablePage extends StatefulWidget {
  const DraggablePage() : super();

  @override
  DraggablePageState createState() => DraggablePageState();
}

class DraggablePageState extends State<DraggablePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Draggable<Color>(
          child: Container(
            height: 200,
            width: 200,
            color: Colors.blueAccent,
          ),
          childWhenDragging: Container(
            height: 200,
            width: 200,
            color: Colors.greenAccent,
          ),
          feedback: Container(
            height: 200,
            width: 200,
            color: Colors.cyanAccent,
          )),
    ));
  }
}
