import 'package:flutter/material.dart';
import 'package:hundred_days_of_flutter_widget/cliprrect.dart';

class HeroWidgetExample extends StatefulWidget {
  HeroWidgetExample() : super();

  @override
  HeroWidgetExampleState createState() => HeroWidgetExampleState();
}

class HeroWidgetExampleState extends State<HeroWidgetExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //backgroundColor: Colors.cyanAccent,
        body: Container(
      alignment: Alignment.bottomCenter,
      child: GestureDetector(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(50),
          child: Hero(
              tag: "heroImage",
              child: Image.asset(
                "images/icon.png",
                width: 70,
                height: 70,
              )),
        ),
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (_) {
            return HeroDetailPage(); // Navigate the detail page
          }));
        },
      ),
    ));
  }
}

// Detail Page

class HeroDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.cyan,
      body: GestureDetector(
        child: Center(
          child: Container(
            height: 250,
            width: 250,
            padding: EdgeInsets.symmetric(
              horizontal: 20.0,
              vertical: 15.0,
            ),
            decoration: BoxDecoration(
                // color: Colors.lightBlueAccent,
                color: Colors.cyan,
                borderRadius: BorderRadius.circular(40.0),
                border: Border.all(color: Colors.blueGrey, width: 2),
                boxShadow: [
                  BoxShadow(
                    color: Colors.blueGrey,
                    blurRadius: 10.0, // soften the shadow
                    spreadRadius: 3.0, //extend the shadow
                    offset: Offset(
                      6.0, // Move to right 10  horizontally
                      6.0, // Move to bottom 10 Vertically
                    ),
                  ),
                ]),
            //color: Colors.lightGreenAccent,
            child: Stack(
              children: <Widget>[
                Hero(
                  tag: "imagehero",
                  child: Column(
                    children: <Widget>[
                      Image.asset("images/sitlogo.png"),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 10.0),
                        child: Text(
                          "Right place for students who want to start great career. Real beauty is this company is driven by students.",
                          style: TextStyle(
                              fontSize: 15,
                              fontStyle: FontStyle.italic,
                              color: Colors.white),
                        ),
                      )
                    ],
                  ),
                )
                // Column(
                //   children: <Widget>[
                //     Image.asset("images/sitlogo.png"),
                //     Text("Hello All"),
                //   ],
                // )
              ],
            ),
          ),

          // child: ClipRRect(
          //   borderRadius: BorderRadius.circular(15.0),
          //   child: Stack(
          //     fit: StackFit.expand,
          //     children: <Widget>[
          //       Column(
          //         mainAxisAlignment: MainAxisAlignment.center,
          //         children: <Widget>[
          //           Stack(
          //             children: <Widget>[
          //               Container(
          //                 height: 240,
          //                 width: 240,
          //                 child: Column(
          //                   children: <Widget>[
          //                     Image.asset("images/sitlogo.png"),
          //                     Text("Hello All"),
          //                   ],
          //                 ),
          //               )
          //             ],
          //           )
          //         ],
          //       )
          //     ],
          //   ),
          // ),
        ),
        onDoubleTap: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}
