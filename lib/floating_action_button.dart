import 'package:flutter/material.dart';

class FAB extends StatefulWidget {
  const FAB() : super();
  @override
  FABState createState() => FABState();
}

class FABState extends State<FAB> {
  var _height = 400.0;
  var _width = 400.0;
  var _opacity = 0.0;

  animateOpacity() {
    setState(() {
      _opacity = _opacity == 0 ? 1.0 : 0.0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // first to create a floating action button
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          animateOpacity();
        },
        child: Icon(Icons.add),
      ),
      //create the BottomNavigationBar
      bottomNavigationBar: BottomAppBar(
        //to marge the FAB and BottomNavigationButton
        shape: CircularNotchedRectangle(),
        color: Colors.black,
        child: Container(
          height: 40.0,
        ),
      ),

      //to set the FAB  plase
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: Container(
        padding: EdgeInsets.all(20.0),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            AnimatedContainer(
              duration: Duration(seconds: 1),
              curve: Curves.bounceIn,
              color: Colors.white,
              height: _height,
              width: _width,
              alignment: Alignment.center,
              child: AnimatedOpacity(
                opacity: _opacity,
                duration: Duration(seconds: 2),
                child: Container(
                    child: Text(
                  "Welcome to Sasurie InfoTech",
                  style: TextStyle(
                      fontSize: 23.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueAccent),
                )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
