import 'package:flutter/material.dart';

class PlaceHolderPage extends StatefulWidget {
  const PlaceHolderPage() : super();

  @override
  PlaceHolderState createState() => PlaceHolderState();
}

class PlaceHolderState extends State<PlaceHolderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
              child: Placeholder(
            color: Colors.red,
          )),
          Expanded(
            child: Row(
              children: <Widget>[
                Flexible(
                  flex: 1,
                  child: Placeholder(
                    color: Colors.green,
                  ),
                ),
                Flexible(
                  flex: 4,
                  child: Placeholder(
                    color: Colors.blue,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
