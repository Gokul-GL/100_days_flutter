import 'package:flutter/material.dart';

class ConstrainedBoxPage extends StatefulWidget {
  const ConstrainedBoxPage() : super();

  @override
  ConstrainedBoxState createState() => ConstrainedBoxState();
}

class ConstrainedBoxState extends State<ConstrainedBoxPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Center(
        child: ConstrainedBox(
            constraints: BoxConstraints(minWidth: 200, minHeight: 200),
            child: RaisedButton(
              color: Colors.greenAccent,
              onPressed: () {},
              child: Text("Click_Me"),
            )),
      )),
    );
  }
}
