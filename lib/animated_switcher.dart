import 'package:flutter/material.dart';

class AnimatedSwitcherPage extends StatefulWidget {
  const AnimatedSwitcherPage() : super();

  @override
  AnimatedSwitcherState createState() => AnimatedSwitcherState();
}

class AnimatedSwitcherState extends State<AnimatedSwitcherPage> {
  Widget myWidget = FirstWidget();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: Material(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AnimatedSwitcher(
                  duration: const Duration(seconds: 1),
                  transitionBuilder:
                      (Widget child, Animation<double> animation) {
                    return ScaleTransition(
                      child: child,
                      scale: animation,
                    );
                  },
                  // transitionBuilder: (Widget child, Animation<double> animation) {
                  //   return RotationTransition(
                  //     child: child,
                  //     turns: animation,
                  //   );
                  // },
                  child: myWidget,
                ),
                RaisedButton(
                  onPressed: () {
                    setState(() {
                      myWidget = (myWidget.toString() == "FirstWidget")
                          ? SecondWidget()
                          : FirstWidget();
                    });
                  },
                  child: Text("Click_Me"),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class FirstWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200.0,
      height: 200.0,
      child: Center(child: Text("First Widget")),
      decoration: BoxDecoration(
        color: Colors.greenAccent,
      ),
    );
  }
}

class SecondWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200.0,
      height: 200.0,
      child: Center(child: Text("Second Widget")),
      decoration: BoxDecoration(
        color: Colors.blueAccent,
      ),
    );
  }
}
