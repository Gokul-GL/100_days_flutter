import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

class PositionExample extends StatefulWidget {
  const PositionExample() : super();

  @override
  PositionExampleState createState() => PositionExampleState();
}

class PositionExampleState extends State<PositionExample> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.cyanAccent,
      margin: EdgeInsets.all(10.0),
      width: 240,
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Positioned(
              bottom: 130,
              child: Container(
                height: 150,
                width: 240,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: EdgeInsets.all(05),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        "16-inch model",
                        style: TextStyle(fontSize: 13, color: Colors.black),
                      ),
                      SizedBox(height: 5.0),
                      Text(
                        "MacBook Pro",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                      SizedBox(height: 5.0),
                      Text(
                        "The best for the brightest.",
                        style: TextStyle(fontSize: 12, color: Colors.black),
                      )
                    ],
                  ),
                ),
              )),
          Align(
            alignment: Alignment.center,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20.0),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0.0, 2.0),
                    blurRadius: 6.0,
                  ),
                ],
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: SizedBox(
                    height: 180.0,
                    width: 220.0,
                    child: Carousel(
                      images: [
                        NetworkImage(
                            'https://i.pinimg.com/originals/1e/d0/b3/1ed0b37a7a2c3cda8fa3e83e4932e199.jpg'),
                        NetworkImage(
                            'https://i.pinimg.com/originals/ef/3e/6d/ef3e6db8e7c714f9a815811a31cf98d7.jpg'),
                        NetworkImage(
                            'https://blog.malwarebytes.com/wp-content/uploads/2016/04/apple-mac-macbook-feature-900x506.jpg'),
                        NetworkImage(
                            'https://c0.wallpaperflare.com/preview/197/297/242/macbook-pro.jpg'),
                      ],
                    )),
              ),
            ),
          )
        ],
      ),
    );
  }
}
