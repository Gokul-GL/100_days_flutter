import 'package:flutter/material.dart';

class SemanticsPage extends StatefulWidget {
  const SemanticsPage() : super();

  @override
  SemanticsState createState() => SemanticsState();
}

class SemanticsState extends State<SemanticsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Center(
        child: Semantics(
          child: Container(
            width: 200,
            height: 200,
            color: Colors.greenAccent,
            child: Center(
              child: Text(
                "Semantics Example by mr_._motoholic_gl_",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.black),
              ),
            ),
          ),
          label: "Container",
          button: false,
          checked: false,
          container: true,
          currentValueLength: 1,
          enabled: true,
          explicitChildNodes: false,
          focusable: false,
          focused: false,
        ),
      )),
    );
  }
}
