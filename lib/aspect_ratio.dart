import 'package:flutter/material.dart';

class AspectRatioPage extends StatefulWidget {
  const AspectRatioPage() : super();

  @override
  AspectRatioState createState() => AspectRatioState();
}

class AspectRatioState extends State<AspectRatioPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: AspectRatio(
          aspectRatio: 3 / 2, //to  give the ratio of your container
          child: Container(
            color: Colors.cyanAccent,
          ),
        ),
      ),
    );
  }
}
