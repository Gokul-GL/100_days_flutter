import 'package:flutter/material.dart';

class MediaQueryPage extends StatefulWidget {
  const MediaQueryPage() : super();

  MediaQueryPageState createState() => MediaQueryPageState();
}

class MediaQueryPageState extends State<MediaQueryPage> {
  @override
  Widget build(BuildContext context) {
    //To get the Screen width useing MediaQuery

    var screenSize = MediaQuery.of(context).size.width;

    return Scaffold(
      body: LayoutBuilder(builder: (context, constraints) {
        if (screenSize < 600) {
          return buildOneLayout();
        } else {
          return buildTwoLayout();
        }
      }),
    );
  }
  //First Layout

  Widget buildOneLayout() {
    /**
     *  Simple Example
     *   // return Center(
    //   child: Container(
    //     height: 300,
    //     width: 300,
    //     color: Colors.amberAccent,
    //   ),
    // );
     *  
     */

    //Alternative Example

    return CustomScrollView(
      slivers: <Widget>[
        SliverFixedExtentList(
            delegate: SliverChildListDelegate(
              [
                Container(
                    color: Colors.blueAccent,
                    child: Center(child: Text('List 1'))),
                Container(child: Center(child: Text('List 2'))),
                Container(
                    color: Colors.greenAccent,
                    child: Center(child: Text('List 3'))),
                Container(
                    color: Colors.grey, child: Center(child: Text('List 4 '))),
                Container(
                    color: Colors.purpleAccent,
                    child: Center(child: Text('List 5'))),
                Container(
                    color: Colors.lightBlueAccent,
                    child: Center(child: Text('List 6'))),
              ],
            ),
            itemExtent: 150)
      ],
    );
  }

//Secont Build Layout

  Widget buildTwoLayout() {
    return CustomScrollView(
      slivers: <Widget>[
        SliverGrid.count(
          crossAxisCount: 2,
          children: [
            Container(
                color: Colors.blueAccent, child: Center(child: Text('List 1'))),
            Container(child: Center(child: Text('List 2'))),
            Container(
                color: Colors.greenAccent,
                child: Center(child: Text('List 3'))),
            Container(
                color: Colors.grey, child: Center(child: Text('List 4 '))),
            Container(
                color: Colors.purpleAccent,
                child: Center(child: Text('List 5'))),
            Container(
                color: Colors.lightBlueAccent,
                child: Center(child: Text('List 6'))),
          ],
        )
      ],
    );
  }
}
