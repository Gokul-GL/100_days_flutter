import 'package:flutter/material.dart';

class StreamBuilderExamble extends StatefulWidget {
  StreamBuilderExamble() : super();

  StreamBuilderState createState() => StreamBuilderState();
}

/**
 * A source of asynchronous data events.
 * A Stream provides a way to receive a sequence of events.
 Each event is either a data event, also called an element of the stream, 
 or an error event, which is a notification that something has failed. 
 When a stream has emitted all its event, a single "done" 
 event will notify the listener that the end has been reached.
 * 
 * 
 * 
 * 
 * 
 */
class StreamBuilderState extends State<StreamBuilderExamble> {
  Stream<int> _bids = (() async* {
    await Future<void>.delayed(Duration(seconds: 1));
    yield 1;
    await Future<void>.delayed(Duration(seconds: 1));
  })();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: FractionalOffset.center,
        color: Colors.white,

        //StreamBuilder

        child: StreamBuilder<int>(
            stream: _bids,
            builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
              List<Widget> children;
              //Stream has get the error or not
              if (snapshot.hasError) {
                //is get the error to execute this part of code otherwise execute the else part
                children = <Widget>[
                  Icon(
                    Icons.error_outline,
                    color: Colors.red,
                    size: 60,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: Text('Error: ${snapshot.error}'),
                  )
                ];
              } else {
                switch (snapshot.connectionState) {
                  //Stream is don't have the state
                  case ConnectionState.none:
                    children = <Widget>[
                      Icon(
                        Icons.info,
                        color: Colors.blue,
                        size: 60,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Text('Error: ${snapshot.error}'),
                      )
                    ];
                    break;
                  //Stream is waiting for the state execution
                  case ConnectionState.waiting:
                    children = <Widget>[
                      SizedBox(
                        child: const CircularProgressIndicator(),
                        width: 60,
                        height: 60,
                      ),
                      const Padding(
                        padding: EdgeInsets.only(top: 16),
                        child: Text('Awaiting bids...'),
                      ),
                    ];
                    break;
                  //Stream state is active or executing
                  case ConnectionState.active:
                    children = <Widget>[
                      Icon(
                        Icons.check_circle_outline,
                        color: Colors.green,
                        size: 60,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Text('\$${snapshot.data}'),
                      )
                    ];
                    break;
                  //Stream State execution is done
                  case ConnectionState.done:
                    children = <Widget>[
                      Icon(
                        Icons.info,
                        color: Colors.blue,
                        size: 60,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Text('\$${snapshot.data} (closed)'),
                      )
                    ];
                    break;
                }
              }
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: children,
              );
            }),
      ),
    );
  }
}
