import 'package:flutter/material.dart';
import 'package:flutter_gifimage/flutter_gifimage.dart';

class TransformPage extends StatefulWidget {
  const TransformPage() : super();

  @override
  TransformPageState createState() => TransformPageState();
}

class TransformPageState extends State<TransformPage>
    with TickerProviderStateMixin {
  GifController controller;
  @override
  void initState() {
    controller = GifController(vsync: this);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.repeat(min: 0, max: 53, period: Duration(milliseconds: 200));
    });
    super.initState();
  }

  Offset _offset = Offset.zero; // changed

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Transform(
        transform: Matrix4.identity()
          ..setEntry(3, 2, 0.001) // perspective
          ..rotateX(0.01 * _offset.dy) // changed
          ..rotateY(-0.01 * _offset.dx), // changed
        alignment: FractionalOffset.center,
        child: GestureDetector(
            onPanUpdate: (details) => setState(() => _offset += details.delta),
            onDoubleTap: () => setState(() => _offset = Offset.zero),
            child: Center(
              child: Image.asset(
                "images/splash2.jpg",
              ),
            )),
      ),
    );
  }
}
