import 'package:flutter/material.dart';

class LayoutBuilderExample extends StatefulWidget {
  const LayoutBuilderExample() : super();

  @override
  LayoutBuilderExampleState createState() => LayoutBuilderExampleState();
}

class LayoutBuilderExampleState extends State<LayoutBuilderExample> {
//Builds a widget tree that can depend on the parent widget's size.

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(builder: (context, constraints) {
        if (constraints.maxWidth > 600) {
          return buildTwoLayout();
        } else {
          return buildOneLayout();
        }
      }),
    );
  }

//First Layout

  Widget buildOneLayout() {
    /**
     *  Simple Example
     *   // return Center(
    //   child: Container(
    //     height: 300,
    //     width: 300,
    //     color: Colors.amberAccent,
    //   ),
    // );
     *  
     */

    //Alternative Example

    return CustomScrollView(
      slivers: <Widget>[
        SliverFixedExtentList(
            delegate: SliverChildListDelegate(
              [
                Container(
                    color: Colors.blueAccent,
                    child: Center(child: Text('List 1'))),
                Container(child: Center(child: Text('List 2'))),
                Container(
                    color: Colors.greenAccent,
                    child: Center(child: Text('List 3'))),
                Container(
                    color: Colors.grey, child: Center(child: Text('List 4 '))),
                Container(
                    color: Colors.purpleAccent,
                    child: Center(child: Text('List 5'))),
                Container(
                    color: Colors.lightBlueAccent,
                    child: Center(child: Text('List 6'))),
              ],
            ),
            itemExtent: 150)
      ],
    );
  }

//Secont Build Layout

  Widget buildTwoLayout() {
    return CustomScrollView(
      slivers: <Widget>[
        SliverGrid.count(
          crossAxisCount: 2,
          children: [
            Container(
                color: Colors.blueAccent, child: Center(child: Text('List 1'))),
            Container(child: Center(child: Text('List 2'))),
            Container(
                color: Colors.greenAccent,
                child: Center(child: Text('List 3'))),
            Container(
                color: Colors.grey, child: Center(child: Text('List 4 '))),
            Container(
                color: Colors.purpleAccent,
                child: Center(child: Text('List 5'))),
            Container(
                color: Colors.lightBlueAccent,
                child: Center(child: Text('List 6'))),
          ],
        )
      ],
    );
  }
}
