import 'package:flutter/material.dart';

class AnimatedPostionedPage extends StatefulWidget {
  const AnimatedPostionedPage() : super();

  @override
  AnimatedPostionedState createState() => AnimatedPostionedState();
}

class AnimatedPostionedState extends State<AnimatedPostionedPage> {
  var height = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
      onTap: () => setState(() {
        height = !height;
      }),
      child: Center(
        child: Stack(
          alignment: Alignment.center,
          children: [
            AnimatedPositioned(
                width: 200,
                height: height ? 300 : 100,
                curve: Curves.elasticOut,
                child: Container(
                  child: Center(
                    child: Text(
                      "Tab_Me",
                      style: TextStyle(color: Colors.black, fontSize: 20),
                    ),
                  ),
                  color: Colors.greenAccent,
                ),
                duration: Duration(seconds: 2))
          ],
        ),
      ),
    ));
  }
}
