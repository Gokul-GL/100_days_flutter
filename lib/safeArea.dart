
import 'package:flutter/material.dart';


class DSafeArea extends StatelessWidget 
{

            const DSafeArea({
              Key key,
            }) : super(key: key);


            @override
            Widget build(BuildContext context) 
            {
              //declaration of SafeArea and interface with the Scaffold
                          SafeArea(
                            child:ListView(),
                            top:true,
                            bottom:true,
                            left:true,
                            right:true,

                          );
                          return Scaffold(
                            //to set the appbar and title of your app
                                appBar: AppBar(
                                  title: Text("SafeArea"),
                                ),
                                  body:SafeArea(
                                    //to print the texts in listview 
                                      child: ListView(
                                          children: List.generate(20, (i) => Text("#Day1 Flutter with SafeArea",style:TextStyle(fontSize:24),))
                                          ),
                                          )
                                          );
              
                
            }             
}
