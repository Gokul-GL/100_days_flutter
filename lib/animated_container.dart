import 'package:flutter/material.dart';

class AnimatedContainerPage extends StatefulWidget {
  const AnimatedContainerPage() : super();

  @override
  AnimatedContainerState createState() => AnimatedContainerState();
}

class AnimatedContainerState extends State<AnimatedContainerPage> {
  var _height = 400.0;
  var _width = 400.0;
  var _opacity = 0.0;

  animateOpacity() {
    setState(() {
      _opacity = _opacity == 0 ? 1.0 : 0.0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(20.0),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            AnimatedContainer(
              duration: Duration(seconds: 1),
              curve: Curves.bounceIn,
              color: Colors.white,
              height: _height,
              width: _width,
              alignment: Alignment.center,
              child: AnimatedOpacity(
                opacity: _opacity,
                duration: Duration(seconds: 2),
                child: Container(
                  child: FlutterLogo(
                    size: 200.0,
                  ),
                ),
              ),
            ),
            OutlineButton(
                child: Text("Click_Me"),
                color: Colors.blueAccent,
                onPressed: () {
                  animateOpacity();
                }),
          ],
        ),
      ),
    );
  }
}
