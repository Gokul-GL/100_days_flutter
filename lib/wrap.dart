import 'package:flutter/material.dart';

class WrapContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          child: Center(
            child: Text(
              'Wrap Widget',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          margin: EdgeInsets.only(right: 48),
        ),
      ),

      //####### Before Wrap the row

      // body: Column(
      //   children: <Widget>[
      //     Row(
      //       children: <Widget>[
      //         chipDesign("C", Color(0xFF4fc3f7)),
      //         chipDesign("C++", Color(0xFFffb74d)),
      //         chipDesign("Java", Color(0xFFff8a65)),
      //         chipDesign("Python", Color(0xFF9575cd)),
      //         chipDesign("C#", Color(0xFF4db6ac)),
      //         chipDesign("Swift", Color(0xFFf06292)),
      //         chipDesign("Scala", Color(0xFFa1887f)),
      //         chipDesign("Ruby", Color(0xFF90a4ae)),
      //         chipDesign("React.js", Color(0xFFba68c8)),
      //       ],
      //     ),
      //   ],
      // ),

      //@@@@@@@ after warp

      body: Column(
        children: <Widget>[
          Wrap(
            spacing: 0.0, // gap between adjacent chips
            runSpacing: 0.0, // gap between lines
            children: <Widget>[
              chipDesign("C", Color(0xFF4fc3f7)),
              chipDesign("C++", Color(0xFFffb74d)),
              chipDesign("Java", Color(0xFFff8a65)),
              chipDesign("Python", Color(0xFF9575cd)),
              chipDesign("C#", Color(0xFF4db6ac)),
              chipDesign("Swift", Color(0xFFf06292)),
              chipDesign("Scala", Color(0xFFa1887f)),
              chipDesign("Ruby", Color(0xFF90a4ae)),
              chipDesign("React.js", Color(0xFFba68c8)),
            ],
          ),
        ],
      ),
    );
  }

  Widget chipDesign(String label, Color color) => Container(
        child: Chip(
          label: Text(
            label,
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          backgroundColor: color,
          elevation: 4,
          shadowColor: Colors.grey[50],
          padding: EdgeInsets.all(4),
        ),
        margin: EdgeInsets.only(left: 12, right: 12, top: 2, bottom: 2),
      );
}
