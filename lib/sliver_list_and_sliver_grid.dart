import 'package:flutter/material.dart';

class SliverLandSliverGrid extends StatefulWidget {
  const SliverLandSliverGrid() : super();
  @override
  SliverLandSliverGridState createState() => SliverLandSliverGridState();
}

class SliverLandSliverGridState extends State<SliverLandSliverGrid> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: Center(child: Text('SliverAppBar')),
            backgroundColor: Colors.black,
            expandedHeight: 200.0,
            floating: false,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              background: Image.asset('images/scrolbg.jpg', fit: BoxFit.cover),
            ),
          ),
          SliverFixedExtentList(
            itemExtent: 150.0,
            delegate: SliverChildListDelegate(
              [
                Container(
                    color: Colors.blueAccent,
                    child: Center(child: Text('SliverList'))),
                Container(child: Center(child: Text('SliverList'))),
                Container(
                    color: Colors.greenAccent,
                    child: Center(child: Text('SliverList'))),
                Container(
                    color: Colors.grey,
                    child: Center(child: Text('SliverList'))),
                Container(
                    color: Colors.purpleAccent,
                    child: Center(child: Text('SliverList'))),
              ],
            ),
          ),

          /**
           * Sliver Grid Code......
           */
          SliverGrid.count(
            crossAxisCount: 3,
            children: [
              Container(
                  color: Colors.blueAccent,
                  child: Center(child: Text('SliverGrid'))),
              Container(child: Center(child: Text('SliverGrid'))),
              Container(
                  color: Colors.greenAccent,
                  child: Center(child: Text('SliverGrid'))),
              Container(
                  color: Colors.grey, child: Center(child: Text('SliverGrid'))),
              Container(
                  color: Colors.purpleAccent,
                  child: Center(child: Text('SliverGrid'))),
              Container(color: Colors.orange, height: 150.0),
              Container(color: Colors.yellow, height: 150.0),
              Container(color: Colors.pink, height: 150.0),
              Container(color: Colors.cyan, height: 150.0),
              Container(color: Colors.indigo, height: 150.0),
            ],
          ),
        ],
      ),
    );
  }
}
