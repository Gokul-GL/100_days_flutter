import 'package:flutter/material.dart';

class FlutterCardView extends StatefulWidget {
  const FlutterCardView() : super();

  @override
  FlutterCardViewState createState() => FlutterCardViewState();
}

class FlutterCardViewState extends State<FlutterCardView>
    with TickerProviderStateMixin {
  GlobalKey<FormState> formkey = new GlobalKey<FormState>();
  AnimationController controller;
  Animation<double> animation;
  initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 7), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);

    /*animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        controller.forward();
      }
    });*/

    controller.forward();
  }

  void validated() {
    if (formkey.currentState.validate()) {
      print("Validated");
    } else {
      print("NOt Validate");
    }
  }

  String validater(value) {
    if (value.isEmpty) {
      return "Required";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          // Image.asset(
          //   'images/loginbg.jpeg',
          //   fit: BoxFit.cover,
          //   color: Colors.grey,
          //   colorBlendMode: BlendMode.darken,
          // ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Login Page",
                    style: TextStyle(
                      fontSize: 50.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  )),
              Stack(
                children: <Widget>[
                  SingleChildScrollView(
                      child: FadeTransition(
                    opacity: animation,
                    child: Form(
                      key: formkey,
                      child: Container(
                        height: 320.0,
                        width: 330.0,
                        padding: EdgeInsets.symmetric(
                          horizontal: 30.0,
                          vertical: 25.0,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.lightBlueAccent,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(40.0),
                          gradient: LinearGradient(
                            colors: [Colors.grey, Colors.black26],
                          ),
                        ),
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 20.0),
                              child: TextFormField(
                                autocorrect: false,
                                autofocus: false,
                                style: TextStyle(fontSize: 15.0),
                                decoration: InputDecoration(
                                    //hintText: "E-mail Id",
                                    border: OutlineInputBorder(),
                                    labelText: "Email id",
                                    filled: true,
                                    fillColor: Colors.grey[200],
                                    contentPadding: EdgeInsets.all(10.0)),
                                validator: validater,
                              ),
                            ),
                            TextFormField(
                              autocorrect: false,
                              autofocus: false,
                              obscureText: true,
                              style: TextStyle(fontSize: 15.0),
                              decoration: InputDecoration(
                                  // hintText: "Password",
                                  border: OutlineInputBorder(),
                                  labelText: "Password",
                                  filled: true,
                                  fillColor: Colors.grey[200],
                                  contentPadding: EdgeInsets.all(10.0)),
                              validator: validater,
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    "Forget Password ?",
                                    style: TextStyle(
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ],
                              ),
                            ),
                            MaterialButton(
                              onPressed: validated,
                              color: Colors.black,
                              minWidth: 250.0,
                              splashColor: Colors.red[200],
                              padding: EdgeInsets.symmetric(vertical: 10.0),
                              child: Text(
                                "Login",
                                style: TextStyle(
                                    fontSize: 18.0, color: Colors.white),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  )),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
