import 'package:flutter/material.dart';


class FlexibleExpendable extends StatelessWidget
{
  const FlexibleExpendable({
              Key key,
            }) : super(key: key);

            @override
            Widget build(BuildContext context)
            {
              return Scaffold(
                body: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:<Widget>[
                    Container(
                      color:Colors.white,
                      height:300,
                      width:double.infinity,
                      child:Center(
                          child: Text("Actual height is 300",style: TextStyle(fontSize:20),)
                      )
                    ),
                    //What you have mentioned the height of the container that will be  ignored by the Expanded
                    Expanded(
                      child: Container(
                        color: Colors.lightBlue,
                        height: 150,
                        width: double.infinity,
                        child: Center(child: Text("height is 150,height ignored by Expanded widget",style: TextStyle(fontSize:20),),),
                      ))

                    //The Container height is completely ignored by Flexible widgets
                    //Takes up remaining space



                    // Flexible(
                    //   fit: FlexFit.tight,
                    //   child: Container(
                    //     color: Colors.lightGreen,
                    //     height: 150,
                    //     width: double.infinity,
                    //     child: Center(child: Text("Flexible Container"),),
                    //   ))
                ]
                ),
              );
            }
} 

