import 'package:flutter/material.dart';
// import 'package:hundred_days_of_flutter_widget/animated_container.dart';

// import 'animated_padding.dart';
// import 'indexed_stack.dart';
import 'constrainedBox.dart';
import 'semantics.dart';
// import 'animated_potioned.dart';
// import 'animated_switcher.dart';
// import 'reorderable_list.dart';
// import 'rich_text.dart';
// import 'stateless_widget.dart';
//import 'package:hundred_days_of_flutter_widget/align_widget.dart';
//import 'package:hundred_days_of_flutter_widget/animated_builder.dart';
//import 'package:hundred_days_of_flutter_widget/position_widget.dart';

//import 'dismissiple.dart';
//import 'sized_box.dart';
//import 'animated_list.dart';
//import 'draggable.dart';
//import 'flexible.dart';
//import 'media_query.dart';
//import 'amimated_Icon.dart';
//import 'aspect_ratio.dart';
//import 'limited_box.dart';
//import 'place_holder.dart';
//import 'spacer.dart';
//import 'value_listner.dart';
//import 'package:hundred_days_of_flutter_widget/back_drop.dart';
//import 'package:hundred_days_of_flutter_widget/absorb_pointer.dart';
//import 'package:hundred_days_of_flutter_widget/transform_widget.dart';
//import 'package:hundred_days_of_flutter_widget/custom_paint.dart';
//import 'package:hundred_days_of_flutter_widget/fitted_box.dart';
//import 'package:hundred_days_of_flutter_widget/layout_builder.dart';
//import 'package:hundred_days_of_flutter_widget/tooltip_widget.dart';
//import 'package:hundred_days_of_flutter_widget/CountProvide_inherited_widget.dart';
//import 'package:hundred_days_of_flutter_widget/cliprrect.dart';
//import 'package:hundred_days_of_flutter_widget/hero_widget.dart';
//import 'package:hundred_days_of_flutter_widget/inherited_widget.dart';
//import 'package:hundred_days_of_flutter_widget/stream_builder.dart';
//import 'package:hundred_days_of_flutter_widget/sliver_list_and_sliver_grid.dart';

//import 'fade_image.dart';
//import 'package:hundred_days_of_flutter_widget/sliver_appBar.dart';
//import 'package:hundred_days_of_flutter_widget/sliverList_and_SliverGrid.dart';

//import 'package:hundred_days_of_flutter_widget/page_view.dart';
//import 'package:hundred_days_of_flutter_widget/table.dart';
//import 'package:hundred_days_of_flutter_widget/flutter_card.dart';

//import 'floating_action_button.dart';
//import 'package:hundred_days_of_flutter_widget/animated_container.dart';
//import 'package:hundred_days_of_flutter_widget/future_builder.dart';
//import 'package:hundred_days_of_flutter_widget/safeArea.dart';
//import 'package:hundred_days_of_flutter_widget/flexible_and_expendable.dart';
//import 'package:hundred_days_of_flutter_widget/wrap.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      // home: HeroWidgetExample(), Day 17
      // home: CustomPaintPage(), Day18
      // home: ToolTipExample(), Day19
      // home: FittedBoxExample(), Day20
      // home: LayoutBuilderExample(), Day21
      // home: AbsorbPointerExample(), Day22
      // home: TransformPage(), Day23
      // home: BackDropExample(), Day24
      // home: AlignExample(), Day25
      // home: PositionExample(), Day26
      // home: AnimatedBuildedExample(), Day27
      //  home: DismissiblePage(), Day28
      //  home: SizedBoxPage(), Day29
      // home: ValueListner(), Day30
      // home: DraggablePage(), Day31
      //  home: AnimatedListExample(), Day32
      //  home: FlexiblePage(), Day33
      // home: MediaQueryPage(), Day34
      // home: SpacerExample(), Day35
      // home: AnimatedIconPage(), Day36
      // home: AspectRatioPage(), Day 37
      // home: LimitedBoxPage(), Day 38
      // home: PlaceHolderPage(), Day 39
      // home: RichTextPage(), Day 40
      // home: ReorderableListPage(), Day 41
      // home: AnimatedSwitcherPage(), Day 42
      // home: AnimatedPostionedPage(), Day 43
      // home: AnimatedPaddingPage(), Day 44
      // home: IndexsedStackPage(), Day 45
      // home: SemanticsPage(), Day 46
      home: ConstrainedBoxPage(),
    );
  }
}
