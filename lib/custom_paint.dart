import 'package:flutter/material.dart';

class CustomPaintPage extends StatefulWidget {
  const CustomPaintPage() : super();

  @override
  CustomPaintPageState createState() => CustomPaintPageState();
}

class CustomPaintPageState extends State<CustomPaintPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Face_CustomPaint"),
      ),
      // Outer white container with padding
      body: Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(horizontal: 40, vertical: 80),
          child: Stack(
            children: <Widget>[
              LayoutBuilder(
                // Inner yellow container
                builder: (_, constraints) => Container(
                  width: constraints.widthConstraints().maxWidth,
                  height: constraints.heightConstraints().maxHeight,
                  color: Colors.yellow,
                  child: CustomPaint(painter: FacePaint()), // Painter Page
                ),
              ),
              Center(
                // child: Text(
                //   "@mr.motoholic_gl_",
                //   style: TextStyle(fontSize: 20),
                // ),
                child: Icon(
                  Icons.navigation,
                  size: 70,
                ),
              ),
              Container(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    children: <Widget>[
                      Text(
                        "@mr.motoholic_gl_",
                        style: TextStyle(fontSize: 20),
                        textAlign: TextAlign.right,
                      ),
                      Icon(
                        Icons.favorite_border,
                        size: 20,
                      )
                    ],
                  ))
            ],
          )),
    );
  }
}

class FacePaint extends CustomPainter {
  @override
  void paint(Canvas canva, Size size) {
    var paint = Paint()
      ..color = Colors.cyanAccent
      ..strokeWidth = 4.0;

    //var strokeCap = StrokeCap.round;

    //Draw a Line

    // Offset startingPoint = Offset(0, size.height / 2);
    // Offset endingPoint = Offset(size.width, size.height / 2);

    // canva.drawLine(startingPoint, endingPoint, paint);

    //Draw a Circule
    // Offset center = Offset(size.width / 2, size.height / 2);
    // canva.drawCircle(center, 100, paint);

    // Face Paint

    // Left eye
    canva.drawRRect(
      RRect.fromRectAndRadius(
          Rect.fromLTWH(20, 40, 100, 100), Radius.circular(20)),
      paint,
    );
    // Right eye
    canva.drawOval(
      Rect.fromLTWH(size.width - 120, 40, 100, 100),
      paint,
    );

    // Mouth
    final mouth = Path();
    mouth.moveTo(size.width * 0.8, size.height * 0.6);
    mouth.arcToPoint(
      Offset(size.width * 0.2, size.height * 0.6),
      radius: Radius.circular(150),
    );
    mouth.arcToPoint(
      Offset(size.width * 0.8, size.height * 0.6),
      radius: Radius.circular(200),
      clockwise: false,
    );

    canva.drawPath(mouth, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
