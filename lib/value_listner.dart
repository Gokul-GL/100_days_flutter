import 'package:flutter/material.dart';

class ValueListner extends StatefulWidget {
  const ValueListner() : super();

  @override
  ValueListnerState createState() => ValueListnerState();
}

class ValueListnerState extends State<ValueListner> {
  final ValueNotifier<int> count = ValueNotifier<int>(0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
              width: double.infinity,
              height: double.infinity,
              child: ValueListenableBuilder(
                  valueListenable: count,
                  builder: (context, value, child) {
                    return Container(
                      child: Stack(
                        children: [
                          Align(
                            alignment: Alignment.topRight,
                            child: Text("top Right :$value"),
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Text("top left :$value"),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text("top Right :$value"),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                RaisedButton(
                                  onPressed: () => count.value += 1,
                                  child: Text("Increment"),
                                ),
                                RaisedButton(
                                  onPressed: () => count.value -= 1,
                                  child: Text("Decrement"),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  }))),
    );
  }
}
