import 'package:flutter/material.dart';

class ToolTipExample extends StatefulWidget {
  const ToolTipExample() : super();
  @override
  ToolTipExampleState createState() => ToolTipExampleState();
}

class ToolTipExampleState extends State<ToolTipExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Tooltip(
            message:
                "Favorite", //to long press the icon ,this message is to popup
            child: Icon(
              Icons.favorite_border,
              size: 80,
            ),
          ),
          Tooltip(
            message:
                "Account_Circle", //to long press the icon ,this message is to popup
            child: Icon(
              Icons.account_circle,
              size: 80,
            ),
          ),
        ],
      )),
    );
  }
}
