import 'package:flutter/material.dart';
import 'package:hundred_days_of_flutter_widget/CountProvide_inherited_widget.dart';
//import 'main.dart';

class InheritedPage extends StatefulWidget {
  InheritedPage() : super();

  @override
  InheritedPageState createState() => InheritedPageState();
}

class InheritedPageState extends State<InheritedPage> {
  var counterProvider;
  void _incrementCounter() {
    setState(() {
      counterProvider.counter.increment();
    });
  }

  @override
  Widget build(BuildContext context) {
    counterProvider = CountProvider.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Inherited Widget'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'button pushed count:',
            ),
            Text(
              '${counterProvider.counter.count}',
              style: Theme.of(context).textTheme.display1, //display the count
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
