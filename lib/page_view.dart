import 'package:flutter/material.dart';

import 'animated_container.dart';
import 'floating_action_button.dart';
import 'flutter_card.dart';

class PageViewExample extends StatefulWidget {
  const PageViewExample() : super();
  @override
  PageViewState createState() => PageViewState();
}

class PageViewState extends State<PageViewExample> {
  final controller = PageController(
    initialPage: 1,
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: PageView(
          controller: controller,
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            // add the pages on hear

            FAB(),
            FlutterCardView(),
            AnimatedContainerPage(),

            // Container(
            //     color: Colors.white, child: Image.asset("im ages/letschat.png")),
            // Container(
            //     color: Colors.black, child: Image.asset("images/loginbg.jpeg")),
            // Container(
            //     color: Colors.white, child: Image.asset("images/sitlogo.png")),
          ],
        ),
      ),
    );
  }
}
