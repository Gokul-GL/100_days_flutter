import 'dart:math';

import 'package:flutter/material.dart';

class AnimatedBuildedExample extends StatefulWidget {
  const AnimatedBuildedExample() : super();

  @override
  AnimatedBuildedState createState() => AnimatedBuildedState();
}

class AnimatedBuildedState extends State<AnimatedBuildedExample>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation _rotationAnim;

  @override
  void initState() {
    super.initState();
    // On init state we actually set the controller and animation....
    _controller =
        AnimationController(vsync: this, duration: Duration(seconds: 5))
          ..repeat(); // the contorller....
    _rotationAnim = Tween(begin: 0.0, end: 2 * pi)
        .animate(_controller); //  the tween animation....
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedBuilder(
        animation: _controller,
        builder: (BuildContext context, Widget child) => Transform.rotate(
          angle: _rotationAnim.value,
        ),
        child: Center(
          // This animation effect is (A 250x250Container going to be rotate at center)....
          child: Container(
            width: 250,
            height: 250,
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}
