import 'package:flutter/material.dart';
import 'package:backdrop/backdrop.dart';

class BackDropExample extends StatefulWidget {
  const BackDropExample() : super();

  @override
  BackDropState createState() => BackDropState();
}

class BackDropState extends State<BackDropExample> {
  @override
  Widget build(BuildContext context) {
    return BackdropScaffold(
      appBar: BackdropAppBar(
        title: Text("Backdrop Example"),
        actions: <Widget>[
          BackdropToggleButton(
            icon: AnimatedIcons.list_view,
          )
        ],
      ),
      backLayer: Center(
        child: Text("Back Layer"),
      ),
      subHeader: BackdropSubHeader(
        title: Text("Sub Header"),
      ),
      frontLayer: Center(
        child: Text("Front Layer"),
      ),
    );
  }
}
