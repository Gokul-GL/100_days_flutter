import 'package:flutter/material.dart';

class AnimatedIconPage extends StatefulWidget {
  const AnimatedIconPage() : super();

  AnimatedIconState createState() => AnimatedIconState();
}

class AnimatedIconState extends State<AnimatedIconPage>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  bool isPlaying = false;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
  }

  void _handleOnPressed() {
    setState(() {
      isPlaying = !isPlaying;
      isPlaying
          ? _animationController.forward()
          : _animationController.reverse();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("AnimatedIcon"),
          centerTitle: true,
        ),
        body: Center(
          child: IconButton(
            iconSize: 140,
            splashColor: Colors.cyanAccent,
            color: Colors.greenAccent,
            icon: AnimatedIcon(
              icon: AnimatedIcons.play_pause,
              progress: _animationController,
            ),
            onPressed: () => _handleOnPressed(),
          ),
        ));
  }
}
