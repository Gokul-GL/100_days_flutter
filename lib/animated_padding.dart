import 'package:flutter/material.dart';

class AnimatedPaddingPage extends StatefulWidget {
  const AnimatedPaddingPage() : super();

  @override
  AnimatedPaddingState createState() => AnimatedPaddingState();
}

class AnimatedPaddingState extends State<AnimatedPaddingPage> {
  double padValue = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: GestureDetector(
        onTap: () => setState(() {
          padValue = padValue + 10;
        }),
        child: AnimatedPadding(
          duration: const Duration(seconds: 1),
          padding: EdgeInsets.all(padValue),
          curve: Curves.elasticIn,
          child: Container(
            height: 400,
            width: 400,
            child: Center(
              child: Text(
                "Tab_Me",
                style: TextStyle(
                    color: Colors.lightBlueAccent,
                    fontStyle: FontStyle.italic,
                    fontSize: 30),
              ),
            ),
            color: Colors.greenAccent,
          ),
        ),
      )),
    );
  }
}
