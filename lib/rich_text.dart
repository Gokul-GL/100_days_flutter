import 'package:flutter/material.dart';

class RichTextPage extends StatefulWidget {
  const RichTextPage() : super();

  @override
  RichTextState createState() => RichTextState();
}

class RichTextState extends State<RichTextPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: RichText(
              text: TextSpan(
                  style: DefaultTextStyle.of(context).style,
                  children: <TextSpan>[
                TextSpan(text: "it's'"),
                TextSpan(text: "all"),
                TextSpan(text: "widget")
              ])),
        ),
      ),
    );
  }
}
