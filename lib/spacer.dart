import 'package:flutter/material.dart';

class SpacerExample extends StatefulWidget {
  const SpacerExample() : super();

  @override
  SpacerExampleState createState() => SpacerExampleState();
}

class SpacerExampleState extends State<SpacerExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          ColoredBox(color: Colors.cyan),
          Spacer(),
          ColoredBox(color: Colors.indigoAccent),
          Spacer(),
          ColoredBox(color: Colors.tealAccent),
        ],
      ),
    );
  }
}
