import 'package:flutter/material.dart';

class FlexiblePage extends StatefulWidget {
  const FlexiblePage() : super();
  FlexiblePageState createState() => FlexiblePageState();
}

class FlexiblePageState extends State<FlexiblePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Flexible(
              flex: 2,
              fit: FlexFit.loose,
              child: Container(
                color: Colors.cyan[200],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Flexible(
              flex: 1,
              child: Container(
                color: Colors.teal[200],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Flexible(
                flex: 3,
                child: Container(
                  color: Colors.indigo[200],
                )),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
